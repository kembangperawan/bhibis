const gulp = require('gulp');
const gulpLoadPlugins = require('gulp-load-plugins');
const browserSync = require('browser-sync').create();
const del = require('del');
const wiredep = require('wiredep').stream;
const runSequence = require('run-sequence');
const inject = require('gulp-inject');
const pugInheritance = require('gulp-pug-inheritance');

const $ = gulpLoadPlugins();
const reload = browserSync.reload;

let dev = true;

let s3_config = {
  accessKeyId: "AKIAJTJ3JXV27HBDCZNQ",
  secretAccessKey: "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"
}
const s3 = require('gulp-s3-upload')(s3_config);

// Upload to AWS S3
gulp.task('s3', () => {
  return gulp.src('./dist/**')
    .pipe(s3({
        // Required
        Bucket: 'stardes.bhinnekalocal.com',
        ACL: 'public-read'
      }, {
        // S3 Constructor Options
        maxRetries: 5
    }));
});

gulp.task('views', () => {
  global.firstRun = global.firstRun === undefined ? true : false;
  return gulp.src(['src/**/*.pug'])

    .pipe($.if( !global.firstRun, $.changed('.tmp', {extension: '.html'}) ))
    .pipe($.cached('pug-files'))

    .pipe($.if( !global.firstRun, pugInheritance({basedir: 'src', extension: '.pug', skip:'node_modules'}) ))

    .pipe($.filter( (file) => {
      return !/\/_/.test(file.path)
    }))

    .pipe($.plumber())
    .pipe($.pug({pretty: true}))
    .pipe(gulp.dest('.tmp'))
    .pipe(reload({stream: true}));
});

gulp.task('styles', () => {
  return gulp.src('src/styles/*.scss')
    .pipe($.plumber())
    .pipe($.if(dev, $.sourcemaps.init()))
    .pipe($.sass.sync({
      outputStyle: 'expanded',
      precision: 10,
      includePaths: ['.']
    }).on('error', $.sass.logError))
    .pipe($.autoprefixer({browsers: ['> 1%', 'last 2 versions', 'Firefox ESR']}))
    .pipe($.if(dev, $.sourcemaps.write()))
    .pipe(gulp.dest('.tmp/styles'))
    .pipe(reload({stream: true}));
});

gulp.task('scripts', () => {
  return gulp.src('src/scripts/**/*.js')
    .pipe($.plumber())
    .pipe($.if(dev, $.sourcemaps.init()))
    .pipe($.babel())
    .pipe($.if(dev, $.sourcemaps.write('.')))
    .pipe(gulp.dest('.tmp/scripts'))
    .pipe(reload({stream: true}));
});

function lint(files) {
  return gulp.src(files)
    .pipe($.eslint({ fix: true }))
    .pipe(reload({stream: true, once: true}))
    .pipe($.eslint.format())
    .pipe($.if(!browserSync.active, $.eslint.failAfterError()));
}

gulp.task('lint', () => {
  return lint('src/scripts/**/*.js')
    .pipe(gulp.dest('src/scripts'));
});
gulp.task('lint:test', () => {
  return lint('test/spec/**/*.js')
    .pipe(gulp.dest('test/spec'));
});

gulp.task('html', ['views', 'styles', 'scripts'], () => {
  return gulp.src(['.tmp/**/*.html', '!src/_*/**'])
    .pipe($.useref({searchPath: ['.tmp', 'src', '.']}))
    .pipe($.if(/\.js$/, $.uglify({compress: {drop_console: true}})))
    .pipe($.if(/\.css$/, $.cssnano({safe: true, autoprefixer: false})))
    .pipe($.if(/\.html$/, $.htmlmin({
      // collapseWhitespace: true,
      // minifyCSS: true,
      // minifyJS: {compress: {drop_console: true}},
      // processConditionalComments: true,
      removeComments: true,
      removeEmptyAttributes: true,
      removeScriptTypeAttributes: true,
      removeStyleLinkTypeAttributes: true,
      collapseBooleanAttributes: true,
      removeRedundantAttributes: true
    })))
    .pipe(gulp.dest('dist'));
});

gulp.task('html:build', ['views', 'styles', 'scripts'], () => {
  return gulp.src(['.tmp/**/*.html', '!src/_*/**'])
    .pipe($.useref({searchPath: ['.tmp', 'src', '.']}))
    .pipe(gulp.dest('dist'));
});

gulp.task('images', () => {
  return gulp.src('src/images/**/*')
    .pipe($.cache($.imagemin()))
    .pipe(gulp.dest('dist/images'));
});

gulp.task('fonts', () => {
  return gulp.src(require('main-bower-files')('**/*.{eot,svg,ttf,woff,woff2}', function (err) {})
    .concat('src/fonts/**/*'))
    .pipe($.if(dev, gulp.dest('.tmp/fonts'), gulp.dest('dist/fonts')));
});

gulp.task('extras', () => {
  return gulp.src([
    'src/*',
    '!src/**/*.pug',
    '!src/**/_*/'
  ], {
    dot: true
  }).pipe(gulp.dest('dist'));
});

gulp.task('clean', del.bind(null, ['.tmp', 'dist']));

gulp.task('clean:tmp', del.bind(null, ['.tmp']));

gulp.task('serve', () => {
  runSequence(['clean', 'wiredep', 'inject'], ['views', 'styles', 'scripts', 'fonts'], () => {
    browserSync.init({
      notify: false,
      port: 3000,
      server: {
        baseDir: ['.tmp', 'src'],
        routes: {
          '/bower_components': 'bower_components'
        }
      }
    });

    gulp.watch([
      'src/*.html',
      'src/images/**/*',
      '.tmp/fonts/**/*'
    ]).on('change', reload);

    gulp.watch('src/**/*.pug', ['views']);
    gulp.watch('src/styles/**/*.scss', ['styles']);
    gulp.watch('src/scripts/**/*.js', ['scripts', 'inject']);
    gulp.watch('src/fonts/**/*', ['fonts']);
    gulp.watch('bower.json', ['wiredep', 'fonts']);
  });
});

gulp.task('serve:dist', ['default'], () => {
  browserSync.init({
    notify: false,
    port: 3000,
    server: {
      baseDir: ['dist']
    }
  });
});

gulp.task('serve:test', ['scripts'], () => {
  browserSync.init({
    notify: false,
    port: 3000,
    ui: false,
    server: {
      baseDir: 'test',
      routes: {
        '/scripts': '.tmp/scripts',
        '/bower_components': 'bower_components'
      }
    }
  });

  gulp.watch('src/scripts/**/*.js', ['scripts']);
  gulp.watch(['test/spec/**/*.js', 'test/index.html']).on('change', reload);
  gulp.watch('test/spec/**/*.js', ['lint:test']);
});

// inject bower components
gulp.task('wiredep', () => {
  gulp.src('src/styles/*.scss')
    .pipe($.filter(file => file.stat && file.stat.size))
    .pipe(wiredep({
      ignorePath: /^(\.\.\/)+/
    }))
    .pipe(gulp.dest('src/styles'));

  gulp.src('src/_layouts/*.pug')
    .pipe(wiredep({
      exclude: ['bootstrap-sass'],
      ignorePath: /^(\.\.\/)*\.\./
    }))
    .pipe(gulp.dest('src/_layouts'));
});

gulp.task('build', ['html:build', 'images', 'fonts', 'extras'], () => {
  return gulp.src('dist/**/*').pipe($.size({title: 'build', gzip: true}));
});

gulp.task('default', () => {
  return new Promise(resolve => {
    dev = false;
    runSequence(['clean', 'wiredep', 'inject'], 'build', resolve);
  });
});

gulp.task('inject', () => {
  // It's not necessary to read the files (will speed up things), we're only after their paths
  var sources = gulp.src(['./src/**/*.js', '!./src/scripts/main.js'], {read: false});

  return gulp.src('src/_layouts/*.pug')
    .pipe(inject(sources, {ignorePath: 'src'}))
    .pipe(gulp.dest('src/_layouts'));
});

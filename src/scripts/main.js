'use strict';

$(function() {

  $('[data-toggle="tooltip"]').tooltip();

  $('a[href="#"]').on('click', function(event){
    event.preventDefault();
  });

  $('.bt-left-container, .bt-right-container').theiaStickySidebar({
    additionalMarginTop: 32
  });

  $('.bt-aside-filter, .bt-product-content').theiaStickySidebar({
    additionalMarginTop: 64
  });

  //Cart
  $(document).on('submit', '.bt-ecoupon-form', function(e){
    e.preventDefault();
    window.location = '/cart/coupon.html#row-coupon';
  });

  $(document).on('click', '.bt-ecoupon-cancel', function(e){
    e.preventDefault();
    window.location = '/cart/#row-coupon';
  });

  //Payment
  //create new address for shipping
  $('#shipmentAddress').on('change', function(){
    // alert($(this).val());
    if ($(this).val() == 4) {
      $('.address-test').removeClass('hide');
    } else {
      $('.address-test').addClass('hide');
    }

  });

  $('#dl-menu').dlmenu();

  //change content on ebanking
  $('#eBanking').on('change', function(){
    // alert($(this).val());
    if ($(this).val() == 0) {
      $('.bt-klikbca').removeClass('hide');
    } else {
      $('.bt-klikbca').addClass('hide');
    }

    if ($(this).val() == 1) {
      $('.bt-bcaklikpay').removeClass('hide');
    } else {
      $('.bt-bcaklikpay').addClass('hide');
    }

    if ($(this).val() == 2) {
      $('.bt-mandiriclickpay').removeClass('hide');
    } else {
      $('.bt-mandiriclickpay').addClass('hide');
    }

    if ($(this).val() == 3) {
      $('.bt-cimbclicks').removeClass('hide');
    } else {
      $('.bt-cimbclicks').addClass('hide');
    }

    if ($(this).val() == 4) {
      $('.bt-epaybri').removeClass('hide');
    } else {
      $('.bt-epaybri').addClass('hide');
    }

    if ($(this).val() == 5) {
      $('.bt-danamononline').removeClass('hide');
    } else {
      $('.bt-danamononline').addClass('hide');
    }

  });

  //change content on virtual account
  $('#vacc').on('change', function(){
    // alert($(this).val());
    if ($(this).val() == 0) {
      $('.bt-vacc-bca').removeClass('hide');
    } else {
      $('.bt-vacc-bca').addClass('hide');
    }

    if ($(this).val() == 1) {
      $('.bt-vacc-permata').removeClass('hide');
    } else {
      $('.bt-vacc-permata').addClass('hide');
    }

  });

  //change content on e-money
  $('#emoney').on('change', function(){
    // alert($(this).val());
    if ($(this).val() == 0) {
      $('.bt-emoney-sakuku').removeClass('hide');
    } else {
      $('.bt-emoney-sakuku').addClass('hide');
    }

    if ($(this).val() == 1) {
      $('.bt-emoney-mandiri').removeClass('hide');
    } else {
      $('.bt-emoney-mandiri').addClass('hide');
    }

  });

  var openPhotoSwipe = function() {

    // parse slide data (url, title, size ...) from DOM elements
    var parseThumbnailElements = function(el) {
      var thumbElements = el.childNodes,
        numNodes = thumbElements.length,
        items = [],
        figureEl,
        linkEl,
        size,
        item;

      for (var i = 0; i < numNodes; i++) {
        figureEl = thumbElements[i]; // <figure> element

        // include only element nodes
        if (figureEl.nodeType !== 1) {
          continue;
        }

        linkEl = figureEl; // <a> element

        size = linkEl.getAttribute('data-size').split('x');

        // create slide object
        item = {
          src: linkEl.getAttribute('data-large'),
          w: parseInt(size[0], 10),
          h: parseInt(size[1], 10)
        };

        if (figureEl.children.length > 1) {
          // <figcaption> content
          item.title = figureEl.children[1].innerHTML;
        }

        if (linkEl.children.length > 0) {
          // <img> thumbnail element, retrieving thumbnail url
          item.msrc = linkEl.children[0].getAttribute('src');
        }

        item.el = figureEl; // save link to element for getThumbBoundsFn
        items.push(item);
      }

      return items;
    };

    var thumbnailSlick = document.querySelector('.bt-photoswipe-gallery .slick-list .slick-track');

    var items = parseThumbnailElements(thumbnailSlick);

    var pswpElement = document.querySelectorAll('.pswp')[0];

    // define photoswipe options
    var options = {
      history: false,
      focus: false,
      showAnimationDuration: 0,
      hideAnimationDuration: 0
    };

    var gallery = new PhotoSwipe( pswpElement, PhotoSwipeUI_Default, items, options);
    gallery.init();
  };

  $(document).on('click', '.breadcrumb-item.--more', function () {
    $(this).toggleClass('--open');
  });

  //Product List - Toggling View
  $('#viewToggle :input').change( function(event) {

    var wrapper = $('main.bt-product-content');
    var viewType = $(this).val();

    if ( viewType === 'grid' ) {
      wrapper.removeClass('--list-view').addClass('--grid-view');
    }
    if ( viewType === 'list' ) {
      wrapper.removeClass('--grid-view').addClass('--list-view');
    }
  });

  if ( $('body#bt-product').length > 0 && $(window).width() <= 767 ) {
    $('.bt-breadcrumb').scrollLeft( 1000 );
  }


  var itemsImages = [];

  function storeImages(key,show) {
    var size = $(this).attr('data-size').split('x');
    itemsImages[key] = {
      src: $(this).attr('data-large'),
      w: size[0],
      h: size[1]
    };
    // console.log(itemsImages);
  }

  $(".slick-big").each(storeImages);

  $('#mainProductDetail').on('click', '#detailImage', function(){
    var swipeIndex = $('#detailImage .slick-current').data('slick-index');
    var totalSlick = $(".js-carousel__product-image").slick("getSlick").slideCount;
    // console.log( 'slick: ' + totalSlick );
    var pswpElement = document.querySelector('.pswp');
    // define photoswipe options
    var options = {
      history: false,
      focus: false,
      showAnimationDuration: 0,
      hideAnimationDuration: 0,
      index: swipeIndex
    };
    var gallery = new PhotoSwipe( pswpElement, PhotoSwipeUI_Default, itemsImages, options);
    gallery.init();

    gallery.listen('close', function() {
      // console.log( 'popup: ' + gallery.getCurrentIndex() );
      var indexClick = gallery.getCurrentIndex();
      $('.js-carousel__product-image').slick('slickGoTo',indexClick);
      if ( totalSlick > 6 ) {
        $('.js-carousel__product-thumbs').slick('slickGoTo',indexClick);
      }
    });

  });


});

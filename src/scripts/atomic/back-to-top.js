'use strict';

// Constructor
$(function() {

  // When arrow is clicked
  $(document).on('click', '.bt-back-to-top', function () {
    $('body,html').animate({
      // Scroll to top of body
      scrollTop: 0
    }, 500);
  });

});

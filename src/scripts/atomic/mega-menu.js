'use strict';

// Constructor
$(function() {

  var megaMenu = document.querySelector('#megamenu');
  var megaMenuLinks = '';
  var megaMenuBoxes = '';

  if (megaMenu !== null) {
    megaMenuLinks = megaMenu.querySelectorAll('.bt-mega-menu__link');
    megaMenuBoxes = megaMenu.querySelectorAll('.bt-mega-menu__right');
  }
  var activeIndex = 0;
  var timer;

  function goToTab(index) {

    if (activeIndex == null) {

      megaMenuLinks[index].classList.add('--is-active');
      megaMenuBoxes[index].classList.add('--is-active');

    } else {

      megaMenuLinks[activeIndex].classList.remove('--is-active');
      megaMenuLinks[index].classList.add('--is-active');

      megaMenuBoxes[activeIndex].classList.remove('--is-active');
      megaMenuBoxes[index].classList.add('--is-active');
    }

    activeIndex = index;
  }

  function handleClick(link, index) {
    link.addEventListener('mouseover', function(event) {
      event.preventDefault();
      timer = setTimeout(function() {
        goToTab(index);
      }, 120);
    });
    link.addEventListener('mouseleave', function(event) {
      clearTimeout(timer);
    });
  }

  for (var i = 0; i < megaMenuLinks.length; i++) {
    var link = megaMenuLinks[i];
    handleClick(link, i);
  }

});

var megaMenuWrapper = $('.bt-navbar__mega-menu');
if ( megaMenuWrapper && $(window).width() >= 992 ) {
  megaMenuWrapper.on('mouseenter', function() {
    document.body.classList.add('--overlay');
  });
  megaMenuWrapper.on('mouseleave', function() {
    document.body.classList.remove('--overlay');
  });
}

'use strict';

// Constructor
$(function() {

  // dropdown with form
  $('.dropdown-menu-form').on('click', function(e) {
    e.stopPropagation();
  });

  // show suggestion - desktop
  $('#nav-search').on('keyup', function() {
    var val = $(this).val();
    var suggestionBox = $(this).siblings('.bt-navbar__autocomplete').children('.bt-autocomplete');

    if (val) {
      suggestionBox.addClass('show');
    } else {
      suggestionBox.removeClass('show');
    }
  });


  var lastScrollY = 0;
  var ticking = false;
  var header = document.getElementById('bt-header');

  // Our animation callback
  function navUpdate() {
    var navMegaMenu  = document.querySelector('.bt-navbar__mega-menu');
    var navTopHeight = 40;

    if ( lastScrollY > navTopHeight ) {
      header.classList.add('navbar-fixed-top');
      header.classList.remove('navbar-static-top');
      document.body.classList.add('--sticky-header');
    } else {
      header.classList.add('navbar-static-top');
      header.classList.remove('navbar-fixed-top');
      document.body.classList.remove('--sticky-header');
    }

    if ( navMegaMenu ) {
      if ( lastScrollY > 80 ) {
        navMegaMenu.classList.add('--is-sticky');
        header.classList.add('--shrink');
      } else {
        navMegaMenu.classList.remove('--is-sticky');
        header.classList.remove('--shrink');
      }
    }


    // allow further rAFs to be called
    ticking = false;
    // console.log(lastScrollY);
  }

  // Calls rAF if it's not already been done already
  function requestTick() {
    if ( !ticking ) {
      requestAnimationFrame(navUpdate);
      ticking = true;
    }
  }

  // Callback for our scroll event - just keeps track of the last scroll value
  function stickyHeader() {
    lastScrollY = window.scrollY;
    requestTick();
  }

  if ( header ) {
    // only listen for scroll events
    window.addEventListener('scroll', stickyHeader, false);
  }

  var navSearch = document.getElementById('nav-search');
  var searchAutocomplete = document.getElementsByClassName('bt-autocomplete');
  var navAccount = document.querySelector('.bt-navbar__user');
  var keepOverlay = false;

  function showOverlay() {

    var bodyHasOverlay = document.body.classList.contains('--overlay');

    if ( bodyHasOverlay && navSearch === document.activeElement ) {
      keepOverlay = true;
      navSearch.blur();
    } else {
      document.body.classList.add('--overlay');
    }
  }

  function hideOverlay() {
    document.body.classList.remove('--overlay');
  }

  function hideSearch() {
    searchAutocomplete[0].classList.remove('show');

    if ( !keepOverlay ) {
      document.body.classList.remove('--overlay');
    }
    keepOverlay = false;
  }

  if ( navSearch && $(window).width() >= 992 ) {
    navSearch.addEventListener('focus', showOverlay, false);
    // navSearch.addEventListener('blur', hideSearch, false);

    // Clicks anything outside input search will hide the dropdown and overlay
    $(document).click( function(event) {
      // dirty hack
      if ( event.target.id !== 'nav-search' ) {
        searchAutocomplete[0].classList.remove('show');
        document.body.classList.remove('--overlay');
      }
    });

    // Clicks within the dropdown won't make it hide itself
    searchAutocomplete[0].click( function(e) {
      e.stopPropagation();
    });
  }


  if (navAccount) {
    navAccount.addEventListener('mouseenter', showOverlay, false);
    navAccount.addEventListener('mouseleave', hideOverlay, false);
  }

  $('#navbar-mobile-search')
    .on('focus', function() {
      document.body.classList.add('--search-mobile');
    })
    .on('blur', function() {
      if ($(this).val() == '') {
        document.body.classList.remove('--search-mobile');
      }
    });

  $('.bt-navbar__btn-cancel')
    .on('click', function() {
      document.getElementById('navbar-mobile-form').reset();
      document.getElementsByClassName('bt-navbar__btn-clear')[0].classList.remove('in');
      document.body.classList.remove('--search-mobile');
    });

  $('.bt-navbar__btn-clear')
    .on('click', function() {
      document.getElementById('navbar-mobile-search').value = '';
      document.getElementById('navbar-mobile-search').focus();
      $(this).removeClass('in');
    });

  // mobile: search input show clear button and suggestion list
  $('#navbar-mobile-search').on('keyup', function() {
    var val = $(this).val();
    var clearButton = $(this).siblings('button');
    var suggestionBox = $('.bt-navbar-mobile__autocomplete').children('.bt-autocomplete');

    if (val) {
      clearButton.addClass('in');
      suggestionBox.addClass('show');
    } else {
      clearButton.removeClass('in');
      suggestionBox.removeClass('show');
    }
  });

});

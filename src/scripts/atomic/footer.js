'use strict';

// Constructor
$(function() {

  $('.footer-menu h6').on('click', function(){
    $(this).siblings('.collapse').toggleClass('in');
    $(this).children('.bt-caret').toggleClass('rotated');
  });

});

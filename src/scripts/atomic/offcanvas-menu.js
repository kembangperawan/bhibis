'use strict';

// Constructor
$(function() {

  $('#toggle-offcanvas-left').on('click', function(){
    document.body.classList.add('--offcanvas-left');
    document.body.classList.add('--overlay');
  });

  $('#toggle-offcanvas-right').on('click', function(){
    document.body.classList.add('--offcanvas-right');
    document.body.classList.add('--overlay');
  });

  $('.bt-overlay').on('click', function(){
    if ($('body').hasClass('--offcanvas-left') || $('body').hasClass('--offcanvas-right')) {
      document.body.classList.remove('--offcanvas-left');
      document.body.classList.remove('--offcanvas-right');
      document.body.classList.remove('--overlay');
    }
  });

});

'use strict';

$(function() {

  // Payment method accordion
  $('input[name="payment"]').on('change', function() {
    $('.panel').removeClass('--active');
    $(this).closest('.panel').addClass('--active');

    var nextUrl = $(this).val();

    $('#btn-next-footer').unbind('click');
    $('#btn-next-footer').attr('href', '/payment/' + nextUrl + '.html');
  });


} );

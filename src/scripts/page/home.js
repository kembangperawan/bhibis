'use strict';

$(function() {

  $('.js-carousel__home').slick({
    autoplay: true,
    fade: true,
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    prevArrow: '<button type="button" class="bt-carousel__nav-prev"><span class="pt-icon-large pt-icon-chevron-left"></span></button>',
    nextArrow: '<button type="button" class="bt-carousel__nav-next"><span class="pt-icon-large pt-icon-chevron-right"></span></button>',
    dots: true
  });

  $('.js-carousel__editorspick').slick({
    infinite: true,
    variableWidth: true,
    prevArrow: $('.bt-carousel__nav-prev.js-nav__editorspick'),
    nextArrow: $('.bt-carousel__nav-next.js-nav__editorspick'),
    responsive: [
      {
        breakpoint: 991,
        settings: 'unslick'
      }
    ]
  });

  $('.js-carousel__recommendation').slick({
    infinite: true,
    variableWidth: true,
    prevArrow: $('.bt-carousel__nav-prev.js-nav__recommendation'),
    nextArrow: $('.bt-carousel__nav-next.js-nav__recommendation'),
    responsive: [
      {
        breakpoint: 991,
        settings: 'unslick'
      }
    ]
  });

  $('.js-carousel__generic').slick({
    infinite: true,
    variableWidth: true,
    prevArrow: $('.bt-carousel__nav-prev.js-nav__generic'),
    nextArrow: $('.bt-carousel__nav-next.js-nav__generic'),
    responsive: [
      {
        breakpoint: 991,
        settings: 'unslick'
      }
    ]
  });

} );

'use strict';

$(function() {

  // Collapse and expand product overview
  $('#mainProductDetail').on('click', '.js-btn-overview', function(){
    $(this).parent().toggleClass('--collapsed --expanded');
  });

  // Sticky image on scroll
  $('.bt-product__wrapper-image, .bt-product__content').theiaStickySidebar({
    additionalMarginTop: 96 // navbar height + page padding top
  });

  $('.js-carousel__rekomendasi').slick({
    infinite: true,
    variableWidth: true,
    prevArrow: $('.bt-carousel__nav-prev._rekomendasi'),
    nextArrow: $('.bt-carousel__nav-next._rekomendasi'),
    responsive: [
      {
        breakpoint: 991,
        settings: 'unslick'
      }
    ]
  });

  $('.js-carousel__terkait').slick({
    infinite: true,
    variableWidth: true,
    prevArrow: $('.bt-carousel__nav-prev._terkait'),
    nextArrow: $('.bt-carousel__nav-next._terkait'),
    responsive: [
      {
        breakpoint: 991,
        settings: 'unslick'
      }
    ]
  });

  $('.js-carousel__product-thumbs').slick({
    asNavFor: '.js-carousel__product-image',
    slidesToShow: 6,
    slidesToScroll: 1,
    infinite: false,
    prevArrow: $('.bt-product__nav-prev'),
    nextArrow: $('.bt-product__nav-next'),
    focusOnSelect: true,
    responsive: [
      {
        breakpoint: 767,
        settings: 'unslick'
      }
    ]
  });

  $('.js-carousel__product-image').slick({
    accessibility: false,
    infinite: false,
    arrows: false,
    fade: true,
    responsive: [
      {
        breakpoint: 767,
        settings: {
          fade: false,
          dots: true
        }
      }
    ]
  });

  if ( $('body#bt-product').length > 0 ) {
    function checkSlickNoSlide(selector) {
      var el = $(selector),
          getSlick = el.slick('getSlick');

      if ( getSlick.slideCount <= getSlick.options.slidesToShow ) {
        el.addClass('slick-no-slide');
      } else {
        el.removeClass('slick-no-slide');
      }
    }

    checkSlickNoSlide('.js-carousel__product-thumbs');
  }

} );
